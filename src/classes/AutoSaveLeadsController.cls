/**
 * Created by Yury on 18.01.2022.
 */

public with sharing class AutoSaveLeadsController {

    @AuraEnabled
    public static List<Lead> getLeads() {
        List<Lead> leads = [
                SELECT Name, Title, Phone
                FROM Lead
                ORDER BY CreatedDate DESC
                LIMIT 50
        ];

        return leads;
    }
}