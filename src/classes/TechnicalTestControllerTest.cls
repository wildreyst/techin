/**
 * Created by Yury on 01.12.2021.
 */

@IsTest
private class TechnicalTestControllerTest {

	@IsTest
	static void getAllSObjectsOptionsTest() {
		List<Object> options = TechnicalTestController.getAllSObjectsOptions();
		System.assert(!options.isEmpty());
	}

	@IsTest
	static void getSObjectInfo() {
		Map<String, Object> allSObjectInfo = TechnicalTestController.getSObjectInfo('Account');
		System.assertNotEquals(allSObjectInfo.get('objectInfo'), null);
		System.assertNotEquals(allSObjectInfo.get('fieldsInfo'), null);
	}
}