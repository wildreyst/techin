/**
 * Created by Yury on 20.01.2022.
 */

@IsTest
private class InlineEditTableControllerTest {
    @TestSetup
    static void setup() {
        List<Account> accounts = new List<Account>();

        for (Integer i = 1 ; i <= 20; i++){
            accounts.add(new Account(Name  = 'Test ' + i));
        }

        insert accounts;
    }

    @IsTest
    static void getAccountsTest() {
        Test.startTest();
        List<Account> accounts = InlineEditTableController.getAccounts();
        Test.stopTest();

        System.assertEquals(false, accounts.isEmpty());
    }
}