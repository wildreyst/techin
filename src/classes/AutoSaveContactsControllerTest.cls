/**
 * Created by Yury on 18.01.2022.
 */

@IsTest
private class AutoSaveContactsControllerTest {
    @TestSetup
    static void setup() {
        Account acc = new Account(Name = 'testAcc');
        insert acc;

        List<Contact> contacts = new List<Contact>();

        for (Integer i = 1 ; i <= 100; i++){
            contacts.add(new Contact(FirstName  = 'Test ' + i, AccountId = acc.Id, LastName = 'TestLastName'));
        }

        insert contacts;
    }

    @IsTest
    static void getLeadsTest() {
        Test.startTest();
        List<Contact> contacts = AutoSaveContactsController.getContacts();
        Test.stopTest();

        System.assertEquals(false, contacts.isEmpty());
    }
}