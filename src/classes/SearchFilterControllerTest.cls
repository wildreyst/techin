/**
 * Created by Yury on 06.01.2022.
 */

@IsTest
private class SearchFilterControllerTest {

    @TestSetup
    static void setup() {
        Account acc = new Account(Name = 'testAcc');
        insert acc;

        List<Contact> contacts = new List<Contact>();

        for (Integer i = 1 ; i <= 100; i++){
            contacts.add(new Contact(FirstName  = 'Test ' + i, AccountId = acc.Id, LastName = 'TestLastName'));
        }

        insert contacts;
    }

    @IsTest
    static void getContactsByNameTest() {
        Test.startTest();
        List<Contact> contacts = SearchFilterController.getContactsByName('test', 0);
        Test.stopTest();

        System.assertEquals(false, contacts.isEmpty());
    }

    @IsTest
    static void getContactsByNameTestWithEmptyName() {
        Test.startTest();
        List<Contact> contacts = SearchFilterController.getContactsByName('', 0);
        Test.stopTest();

        System.assertEquals(false, contacts.isEmpty());
    }
}