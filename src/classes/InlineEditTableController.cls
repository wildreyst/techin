/**
 * Created by Yury on 20.01.2022.
 */

public with sharing class InlineEditTableController {

    @AuraEnabled
    public static List<Account> getAccounts() {
        List<Account> accounts = [
                SELECT Name, Rating
                FROM Account
                ORDER BY CreatedDate DESC
                LIMIT 10
        ];

        return accounts;
    }
}