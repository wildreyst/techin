/**
 * Created by Yury on 18.01.2022.
 */

public with sharing class AutoSaveContactsController {

    @AuraEnabled
    public static List<Contact> getContacts() {
        List<Contact> contacts = [
                SELECT Name
                FROM Contact
                ORDER BY CreatedDate DESC
                LIMIT 50
        ];

        return contacts;
    }
}