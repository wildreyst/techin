/**
 * Created by Yury on 06.01.2022.
 */

public with sharing class SearchFilterController {

    @AuraEnabled
    public static List<Contact> getContactsByName(String name, Integer offset) {
        List<Contact> contacts = new List<Contact>();

        if (name != '') {
            String key = '%' + name + '%';
            contacts = [
                    SELECT FirstName, LastName, Email, Account.Name, MobilePhone, CreatedDate
                    FROM Contact
                    WHERE Name LIKE :key
                    ORDER BY CreatedDate DESC
                    LIMIT 20 OFFSET :offset
            ];
        } else {
            contacts = [
                    SELECT FirstName, LastName, Email, Account.Name, MobilePhone, CreatedDate
                    FROM Contact
                    ORDER BY CreatedDate DESC
                    LIMIT 20 OFFSET :offset
            ];
        }

        return contacts;
    }
}