/**
 * Created by Yury on 18.01.2022.
 */

@IsTest
private class AutoSaveLeadsControllerTest {
    @TestSetup
    static void setup() {
        List<Lead> leads = new List<Lead>();

        for (Integer i = 1 ; i <= 100; i++){
            leads.add(new Lead(
                    Company = 'Test Lead',
                    LastName = 'Lead Last Name',
                    Status = 'Open')
            );
        }

        insert leads;
    }

    @IsTest
    static void getLeadsTest() {
        Test.startTest();
        List<Lead> leads = AutoSaveLeadsController.getLeads();
        Test.stopTest();

        System.assertEquals(false, leads.isEmpty());
    }
}