/**
 * Created by Yury on 01.12.2021.
 */

public with sharing class TechnicalTestController {

	@AuraEnabled(Cacheable = true)
	public static List<Object> getAllSObjectsOptions() {
		List<Object> options = new List<Object>();

		for (Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
			Schema.DescribeSObjectResult objResult = o.getDescribe();

			options.add(
				new Map<String, String>{
					'label' => objResult.getLabel(),
					'value' => objResult.getName()
				}
			);
		}

		return options;
	}

	@AuraEnabled
	public static Map<String, Object> getSObjectInfo(String apiName) {
		Map<String, Object> allSObjectInfo = new Map<String, Object>();

		SObjectType objToken = Schema.getGlobalDescribe().get(apiName);
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap();

		Set<String> fieldSet = fields.keySet();
		List<Map<String, String>> fieldsInfo = new List<Map<String, String>>();

		for(String field : fieldSet) {
			Map<String, String> fieldInfo = new Map<String, String>();
			DescribeFieldResult selectedField = fields.get(field).getDescribe();
			fieldInfo.put('name', selectedField.getName());
			fieldInfo.put('label', selectedField.getLabel());
			fieldInfo.put('accessible', String.valueOf(selectedField.isAccessible()));
			fieldsInfo.add(fieldInfo);
		}

		Map<String, String> objectInfo = new Map<String, String>();
		objectInfo.put('keyPrefix', objDef.keyPrefix);
		objectInfo.put('label', objDef.label);
		objectInfo.put('name', objDef.name);
		objectInfo.put('accessible', String.valueOf(objDef.accessible));
		objectInfo.put('updateable', String.valueOf(objDef.updateable));
		objectInfo.put('custom', String.valueOf(objDef.custom));
		objectInfo.put('queryable', String.valueOf(objDef.queryable));

		allSObjectInfo.put('objectInfo', objectInfo);
		allSObjectInfo.put('fieldsInfo', fieldsInfo);

		return allSObjectInfo;
	}
}