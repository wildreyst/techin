/**
 * Created by Yury on 01.12.2021.
 */

import {LightningElement, wire} from 'lwc';
import {ShowToastEvent} from "lightning/platformShowToastEvent";
import getAllSObjectsOptions from '@salesforce/apex/TechnicalTestController.getAllSObjectsOptions';
import getSObjectInfo from '@salesforce/apex/TechnicalTestController.getSObjectInfo';

export default class TechnicalTest extends LightningElement {

	objectInfo = {};
	fieldsInfo = {};
	objectsOptions;

	isLoading = true;
	isLoadingObject = false;

	get showObjectInfo() {
		return !this.isLoadingObject && Object.keys(this.objectInfo).length !== 0
	}

	get showFieldsInfo() {
		return !this.isLoadingObject && this.fieldsInfo.length !== 0
	}

	@wire(getAllSObjectsOptions)
	allObjectsOptions({ error, data }) {
		if (data) {
			this.objectsOptions = data;
			this.isLoading = false;
		} else if (error) {
			this.showToast('Error!', this.reduceErrors(error), 'error');
		}
	};

	handleChangeObject(event) {
		this.isLoadingObject = true;

		getSObjectInfo({
			apiName: event.detail.value
		})
			.then(result => {
				this.objectInfo = result.objectInfo;
				this.fieldsInfo = result.fieldsInfo;
			})
			.catch(error => {
				console.log(error);
				this.showToast('Error!', error.message, 'error');
			})
			.finally(() => {
				this.isLoadingObject = false;
			});
	}

	showToast(title, message, variant) {
		let toastEvent = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant
		});
		this.dispatchEvent(toastEvent);
	}

	reduceErrors(error) {
		let errorMessage;
		if (Array.isArray(error.body)) {
			errorMessage = error.body.map(e => e.message).join(', ');
		} else if (typeof error.body.message === 'string') {
			errorMessage = error.body.message;
		}

		return errorMessage;
	}
}