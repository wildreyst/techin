/**
 * Created by Yury on 17.01.2022.
 */

import {api, LightningElement} from 'lwc';
import {ShowToastEvent} from "lightning/platformShowToastEvent";

export default class NewContactModal extends LightningElement {
    @api visible; //used to hide/show modal

    handleClickCancel() {
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    handleSuccess(event) {
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success',
            message: 'Contact successfully created',
            variant: 'success'
        }));

        this.dispatchEvent(new CustomEvent('new', { detail: event.detail }));
    }
}