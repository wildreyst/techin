/**
 * Created by Yury on 20.01.2022.
 */

import {LightningElement, track, wire} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord, deleteRecord } from 'lightning/uiRecordApi';
import {getObjectInfo, getPicklistValues} from "lightning/uiObjectInfoApi";

import RATING_FIELD from '@salesforce/schema/Account.Rating';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';

import getAccounts from '@salesforce/apex/InlineEditTableController.getAccounts';

export default class InlineEditTableCustom extends LightningElement {
    @track data = [];
    error;
    isEditing = false;
    recordToUpdate = {};

    @wire(getObjectInfo, { objectApiName: ACCOUNT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, {
        recordTypeId: '$objectInfo.data.defaultRecordTypeId',
        fieldApiName: RATING_FIELD
    })
    wiredRatingPicklist

    connectedCallback() {
        this.updateDatatable()
    }

    deleteAccount(event) {
        deleteRecord(event.detail)
            .then(() => {
                this.updateDatatable()
                this.showToast('Success', 'Record deleted', 'success');
            })
            .catch(error => {
                this.showToast('Error deleting record', error.body.message, 'error');
            }).finally(() => {
                this.recordToUpdate = {}
            });
    }

    updateDatatable() {
        getAccounts()
            .then(result => {
                this.data = result;

                if(this.recordToUpdate?.Id) {
                    this.template.querySelectorAll('c-inline-edit-table-row')[this.data.findIndex(item => item.Id === this.recordToUpdate.Id)].closeEditFields();
                }
            })
            .catch(error => {
                this.error = error.message;
            })
    }

    handleSave() {
        updateRecord({fields: this.recordToUpdate})
            .then(() => {
                this.updateDatatable();
                this.isEditing = false;
                this.showToast('Success', 'Lead Updated Successfully', 'success');
            }).catch(error => {
                this.showToast('Error updating or refreshing records', error.body.message, 'error');
            })
    }

    handleEdit(event) {
        this.recordToUpdate = event.detail
        this.isEditing = true;
    }

    handleCancel() {
        this.template.querySelectorAll('c-inline-edit-table-row')[this.data.findIndex(item => item.Id === this.recordToUpdate.Id)].closeEditFields();
        this.recordToUpdate = {}
        this.isEditing = false;
    }

    handleChange(event) {
        this.recordToUpdate = event.detail
    }

    showToast(title, message, variant) {
        let toastEvent = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(toastEvent);
    }
}