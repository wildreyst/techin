/**
 * Created by Yury on 06.01.2022.
 */

import {LightningElement, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { deleteRecord } from 'lightning/uiRecordApi';
import getContactsByName from '@salesforce/apex/SearchFilterController.getContactsByName';

const columns = [
    { label: 'First Name', fieldName: 'FirstName' },
    { label: 'Last Name', fieldName: 'LastName' },
    { label: 'Email', fieldName: 'Email', type: 'email' },
    { label: 'Account', fieldName: 'accountUrl' , type: 'url',
        typeAttributes: {
            label: {
                fieldName: 'accountName'
            }
        }},
    { label: 'Phone', fieldName: 'MobilePhone', type: 'phone' },
    { label: 'Created Date', fieldName: 'CreatedDate', type: 'date',
        typeAttributes:{
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        }
    },
    {
        type: 'button',
        initialWidth: 110,
        typeAttributes: {
            label: 'Delete',
            name: 'Delete',
            title: 'Delete',
            disabled: false,
            value: 'delete',
        },
    }
];

export default class SearchFilter extends LightningElement {
    @track data = [];
    name = '';
    nameNew = '';
    columns = columns;
    offset = 0;
    offsetDefault = 20;
    error;
    baseUrl = `https://${location.host}/`;
    targetDatatable;
    recordIdForDelete;

    isConfirmationModalVisible = false
    isNewContactModalVisible = false

    connectedCallback() {
        this.updateDatatable()
    }

    updateDatatable() {
        getContactsByName({
            name: this.name,
            offset: this.offset
        })
            .then(result => {
                console.log(result)
                let data = JSON.parse(JSON.stringify(result));

                data.forEach(el => {
                    if (el.Account?.Name) {
                        el.accountName = el.Account.Name
                        el.accountUrl = this.baseUrl + el.Account.Id
                    }
                });

                this.data = [...this.data, ...data];

                if (this.targetDatatable) {
                    this.targetDatatable.isLoading = false;
                    if (data.length !== this.offsetDefault) {
                        this.targetDatatable.enableInfiniteLoading = false;
                    }
                }
            })
            .catch(error => {
                this.error = error.message;
            })
    }

    handleFilter() {
        this.data = [];
        this.offset = 0;
        this.name = this.nameNew;
        if (this.targetDatatable) {
            this.targetDatatable.isLoading = true;
            this.targetDatatable.enableInfiniteLoading = true;
        }
        this.updateDatatable()
    }

    handleLoadMore(event) {
        event.preventDefault();
        event.target.isLoading = true;
        this.offset = this.offset + this.offsetDefault;
        this.targetDatatable = event.target;
        this.updateDatatable()
    }

    callRowAction(event) {
        const recId =  event.detail.row.Id;
        const actionName = event.detail.action.name;
        if ( actionName === 'Delete' ) {
            this.recordIdForDelete = recId;
            this.isConfirmationModalVisible = true;
        }
    }

    handleConfirmationModal() {
        this.delete()
        this.isConfirmationModalVisible = false;
    }


    delete() {
        deleteRecord(this.recordIdForDelete)
            .then(() => {
                this.offset--
                this.data = this.data.filter(row => row.Id !== this.recordIdForDelete);
                this.showToast('Success', 'Record deleted', 'success');
            })
            .catch(error => {
                this.showToast('Error deleting record', error.body.message, 'error');
            });
    }

    addNewContact(event) {
        this.offset++
        this.isNewContactModalVisible = false;
        let fields = event.detail.fields;
        let newContact = {};

        if (fields) {
            newContact.Id = event.detail.id
            newContact.LastName = fields.LastName.value
            newContact.FirstName = fields.FirstName.value
            newContact.MobilePhone = fields.MobilePhone.value
            newContact.Email = fields.Email.value
            newContact.CreatedDate = fields.CreatedDate.value
            newContact.accountName = fields.Account.displayValue
            newContact.accountUrl = this.baseUrl + fields.Account.value.id
        }

        this.data = [newContact, ...this.data];
    }

    handleNewContact() {
        this.isNewContactModalVisible = true;
    }

    handleChangeName(event) {
        this.nameNew = event.target.value;
    }

    closeConfirmationModal() {
        this.isConfirmationModalVisible = false;
    }

    closeNewContactModal() {
        this.isNewContactModalVisible = false;
    }

    showToast(title, message, variant) {
        let toastEvent = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(toastEvent);
    }
}