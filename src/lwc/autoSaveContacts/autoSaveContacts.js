/**
 * Created by Yury on 18.01.2022.
 */

import {LightningElement, track} from 'lwc';
import getContacts from '@salesforce/apex/AutoSaveContactsController.getContacts';

const columns = [
    { label: 'Name', fieldName: 'recordUrl' , type: 'url',
        typeAttributes: {
            label: {
                fieldName: 'Name'
            }
        }
    },
    { label: 'idName', fieldName: 'idName' },
];

export default class AutoSaveContacts extends LightningElement {
    @track data = [];
    columns = columns;
    error;

    connectedCallback() {
        this.updateDatatable()
    }

    updateDatatable() {
        getContacts()
            .then(result => {
                let data = JSON.parse(JSON.stringify(result));
                data.forEach(el => {
                    el.recordUrl = `/${el.Id}`;
                    el.idName = `${el.Id}${el.Name}`
                });

                this.data = [ ...data];
            })
            .catch(error => {
                this.error = error.message;
            })
    }
}