/**
 * Created by Yury on 20.01.2022.
 */

import {LightningElement, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { deleteRecord } from 'lightning/uiRecordApi';

import getAccounts from '@salesforce/apex/InlineEditTableController.getAccounts';


const columns = [
    { label: 'Name', fieldName: 'Name', editable: true },
    { label: 'Rating', fieldName: 'Rating', editable: true},
    {
        type: 'button-icon',
        initialWidth: 80,
        typeAttributes:
            {
                iconName: 'utility:delete',
                name: 'Delete',
                variant: 'container'
            }
    },
];

export default class InlineEditTable extends LightningElement {
    @track data = [];
    saveDraftValues = [];
    columns = columns;
    error;


    connectedCallback() {
        this.updateDatatable()
    }

    updateDatatable() {
        getAccounts()
            .then(result => {
                this.data = result;
            })
            .catch(error => {
                this.error = error.message;
            })
    }

    callRowAction(event) {
        const recId = event.detail.row.Id;
        const actionName = event.detail.action.name;
        if (actionName === 'Delete') {
            this.delete(recId)
        }
    }

    delete(recId) {
        deleteRecord(recId)
            .then(() => {
                this.updateDatatable()
                this.showToast('Success', 'Record deleted', 'success');
            })
            .catch(error => {
                this.showToast('Error deleting record', error.body.message, 'error');
            });
    }

    handleSave(event) {
        this.saveDraftValues = event.detail.draftValues;
        const recordInputs = this.saveDraftValues.slice().map(draft => {
            const fields = Object.assign({}, draft);
            return { fields };
        });

        const promises = recordInputs.map(recordInput => updateRecord(recordInput));
        Promise.all(promises).then(res => {
            this.updateDatatable();
            this.showToast('Success', 'Leads Updated Successfully', 'success');
        }).catch(error => {
            this.showToast('Error updating or refreshing records', error.body.message, 'error');
        }).finally(() => {
            this.saveDraftValues = [];
        });
    }

    showToast(title, message, variant) {
        let toastEvent = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(toastEvent);
    }
}