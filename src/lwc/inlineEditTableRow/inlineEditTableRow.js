/**
 * Created by Yury on 20.01.2022.
 */

import {LightningElement, api} from 'lwc';

export default class InlineEditTableRow extends LightningElement {

    @api account;
    @api isEditing;
    @api wiredRatingPicklist

    editRating = false;
    editName = false;
    accountOld;

    get editTitle() {
        return `Edit ${this.account.Name}`
    }

    get cellName() {
        return this.editName ? 'slds-cell-edit cell-selected' : 'slds-cell-edit'
    }

    get cellRating() {
        return this.editRating ? 'slds-cell-edit cell-selected' : 'slds-cell-edit'
    }

    handleDelete() {
        this.dispatchEvent(new CustomEvent('delete', {detail: this.account.Id}));
    }

    handleEdit(event) {
        this.accountOld = JSON.parse(JSON.stringify(this.account))
        const cellName = event.target.value;

        if (cellName === 'rating') {
            this.editRating = true;
        } else if (cellName === 'name') {
            this.editName = true;
        }

        this.dispatchEvent(new CustomEvent('edit', {
            detail: this.account
        }));
    }

    handleName(event) {
        this.account = {...this.account, Name: event.target.value}
        this.dispatchEvent(new CustomEvent('update', {
            detail: this.account
        }));
    }

    handleRating(event) {
        this.account = {...this.account, Rating: event.target.value}
        this.dispatchEvent(new CustomEvent('update', {
            detail: this.account
        }));
    }

    @api
    closeEditFields() {
        this.account = JSON.parse(JSON.stringify(this.accountOld))
        this.editRating = false;
        this.editName = false;
    }
}