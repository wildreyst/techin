/**
 * Created by Yury on 17.01.2022.
 */

import {LightningElement, api} from 'lwc';

export default class ConfirmationModal extends LightningElement {
    @api visible; //used to hide/show modal
    @api title; //modal title
    @api message; //modal message
    @api confirmLabel; //confirm button label
    @api cancelLabel; //cancel button label

    handleClickConfirm() {
        this.dispatchEvent(new CustomEvent('confirm'));
    }

    handleClickCancel() {
        this.dispatchEvent(new CustomEvent('cancel'));
    }
}