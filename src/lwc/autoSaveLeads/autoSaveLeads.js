/**
 * Created by Yury on 18.01.2022.
 */

import {LightningElement, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import getLeads from '@salesforce/apex/AutoSaveLeadsController.getLeads';

const columns = [
    { label: 'Name', fieldName: 'recordUrl' , type: 'url',
        typeAttributes: {
            label: {
                fieldName: 'Name'
            }
        }
    },
    { label: 'Title', fieldName: 'Title', editable: true },
    { label: 'Phone', fieldName: 'Phone', type: 'phone', editable: true },
];

export default class AutoSaveLeads extends LightningElement {
    @track data = [];
    columns = columns;
    error;
    draftValues = [];

    connectedCallback() {
        this.updateDatatable()
    }

    updateDatatable() {
        getLeads()
            .then(result => {
                let data = JSON.parse(JSON.stringify(result));
                data.forEach(el => {
                    el.recordUrl = `/${el.Id}`;
                });

                this.data = [ ...data];
            })
            .catch(error => {
                this.error = error.message;
            })
            .finally(() => {
                if (this.template.querySelector('lightning-datatable')) {
                    this.template.querySelector('lightning-datatable').draftValues = [];
                }
            })
    }

    handleInlineEdit() {
        let draftValues = this.template.querySelector('lightning-datatable').draftValues;
        const inputsItems = draftValues.slice().map(draft => {
            const fields = Object.assign({}, draft);
            return { fields };
        });

        const promises = inputsItems.map(recordInput => updateRecord(recordInput));
        Promise.all(promises).then(res => {
            this.showToast('Success', 'Leads Updated Successfully', 'success');
            this.updateDatatable();
        }).catch(error => {
            this.showToast('Error updating or refreshing records', error.body.message, 'error');
        })
    }

    showToast(title, message, variant) {
        let toastEvent = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(toastEvent);
    }
}